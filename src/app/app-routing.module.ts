import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Page404Component } from './page404/page404.component';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { FirstpageComponent } from './firstpage/firstpage.component';
const routes: Routes = [
    {
        path: '', component: AppComponent, children: [
            { path: '', component: FirstpageComponent },
            { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
            { path: 'order', loadChildren: './order/order.module#OrderModule' }
        ]
    },
    { path: '**', component: Page404Component }
];


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes)
    ],
    declarations: []
})
export class AppRoutingModule { }
