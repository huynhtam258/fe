import { Table } from 'src/@core/models/table';
export const dataTable: Table[] = [
    {
        IdTable: 1,
        NameTable: 'Table 1',
        StatusTable: '',
        NameCus: '',
        Order: [],
    },
    {
        IdTable: 2,
        NameTable: 'Table 2',
        StatusTable: '',
        NameCus: '',
        Order: [],
    },
    {
        IdTable: 3,
        NameTable: 'Table 3',
        StatusTable: '',
        NameCus: '',
        Order: [],
    },
    {
        IdTable: 4,
        NameTable: 'Table 4',
        StatusTable: '',
        NameCus: '',
        Order: [],
    },
    {
        IdTable: 5,
        NameTable: 'Table 5',
        StatusTable: '',
        NameCus: '',
        Order: [],
    },
    {
        IdTable: 6,
        NameTable: 'Table 6',
        StatusTable: '',
        NameCus: '',
        Order: [],
    },
];
