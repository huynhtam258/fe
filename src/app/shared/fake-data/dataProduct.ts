import { Product } from 'src/@core/models/product';

// import { Product } from '../models/product';
export const dataProduct: Product[] = [
    {
        ProductId: 1,
        ProductName: 'Mocha Coffee',
        ProductInfo: 'This is Mocha Coffee',
        ProductPrice: 65000,
    },
];
