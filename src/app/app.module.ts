import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { OnboardingComponent } from './auth/onboarding/onboarding.component';
import { Page404Component } from './page404/page404.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';

import { ProductService } from '../@core/http/product/product.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CoreModule } from 'src/@core/core.module';
import { BillService } from 'src/@core/http/bill/bill.service';
import { ToastrModule } from 'ngx-toastr';
import { Interceptor } from 'src/@core/http/config/http-interceptor';
import { LoginService } from 'src/@core/http';
import { FirstpageComponent } from './firstpage/firstpage.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
// import { ListtableComponent } from './order/listtable/listtable.component';


@NgModule({
  declarations: [
    AppComponent,
    OnboardingComponent,
    Page404Component,
    FirstpageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyA0yf6M7UW1T_5dWTPohf7o-ObD4SHGLys",
      authDomain: "baocao-f4350.firebaseapp.com",
      databaseURL: "https://baocao-f4350.firebaseio.com",
      projectId: "baocao-f4350",
      storageBucket: "baocao-f4350.appspot.com",
      messagingSenderId: "35455263198",
      appId: "1:35455263198:web:a4e6ad79b0e549cb"
    }),
    AngularFireStorageModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      preventDuplicates: false
    }),
    CoreModule.forRoot()
  ],
  providers: [
    LoginService,
    ProductService,
    BillService,
    {
      provide : HTTP_INTERCEPTORS,
      useClass : Interceptor,
      multi : true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
