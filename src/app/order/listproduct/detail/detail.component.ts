import { Component, OnInit, Input } from '@angular/core';
// import { Product } from '../../../../shared/models/product';
import { Router } from '@angular/router';
import { ProductService } from 'src/@core/http/product/product.service';
import { Product } from 'src/@core/models/product';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  detailProduct: Product;
  // @Input() product: Product;

  constructor(private router: Router, private productService: ProductService) { }


  ngOnInit() {

    this.detailProduct = this.productService.detail;

    // console.log('Detail coming:', this.detailProduct);
    // console.log(this.product);
  }


  home() {
    this.router.navigate(['order/productlist']);
  }
}
