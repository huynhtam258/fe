import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from 'src/@core/http/product/product.service';
import { Router } from '@angular/router';
import { Product } from 'src/@core/models/product';
import * as $ from 'jquery';
import { BillService } from 'src/@core/http/bill/bill.service';
// import { count } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { TableService } from 'src/@core/http/table/table.service';
import { Table } from 'src/@core/models/table';
import { element } from '@angular/core/src/render3/instructions';
import { from } from 'rxjs';

@Component({
  selector: 'app-menutablist',
  templateUrl: './menutablist.component.html',
  styleUrls: ['./menutablist.component.scss']
})
export class MenutablistComponent implements OnInit {

  constructor(private router: Router, private productService: ProductService) { }
  @Input() product: Product;
  @Input() nameProduct: string;
  @Input() priceProduct: number;
  @Input() idProduct: any;
  @Input() imageProduct: String;
  @Input() TypeProductId;
  selectedProduct: Product;
  ngOnInit() {
    // $(".textinfo").text($(this).text().substr(0, 150) + '...');
  }
  deleteProduct(id){
    const data = {
      id:id
    }
    this.productService.deleteProduct(data).subscribe((data)=>{
      this.productService.getProductsByIDtypeProduct(this.TypeProductId).subscribe((data)=>{})
    })
    console.log(id);
    
  }
  detail(product) {
    this.productService.getDetail(this.product);
    this.router.navigate(['order/productlist/detail']);
  }

}
