import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenutablistComponent } from './menutablist.component';

describe('MenutablistComponent', () => {
  let component: MenutablistComponent;
  let fixture: ComponentFixture<MenutablistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenutablistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenutablistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
