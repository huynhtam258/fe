import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators/map';
import { finalize } from 'rxjs/operators';
import * as $ from 'jquery';
import { ProductService } from 'src/@core/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.scss']
})
export class ListproductComponent implements OnInit {
  ahihi: FormGroup;
  hinh: String;
  listProduct: any = [];
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadState: Observable<string>;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;
  constructor(private spinner: NgxSpinnerService, private router: Router, private afStorage: AngularFireStorage, private productService: ProductService, private toastr: ToastrService) { }

  ngOnInit() {
    $('.ask').hide();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
    // console.log(this.tokenService.getToken());
    $('.grid').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });

    $('.list').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });
    this.createForm()
    this.getAllProducts();
  }
  getAllProducts() {
    this.productService.getAllProducts().subscribe((dataProduct: any) => {
      this.listProduct = dataProduct;
      console.log(this.listProduct);

    })
  }
  createForm() {
    this.ahihi = new FormGroup({
      ten: new FormControl(),
      gia: new FormControl(),
      loai: new FormControl()
    })
  }
  allProduct() {
    this.router.navigate(['/order/showProduct'])
  }
  upload(event) {
    const id = Math.random().toString(36).substring(2);
    this.ref = this.afStorage.ref(id);
    this.task = this.ref.put(event.target.files[0]);
    this.uploadState = this.task.snapshotChanges().pipe(map(s => s.state));
    this.uploadProgress = this.task.percentageChanges();
    this.task.snapshotChanges().pipe(
      finalize(() => {
        this.ref.getDownloadURL().subscribe(url => {
          this.hinh = url
          console.log(url);
        });
      })
    ).subscribe();

  }
  add() {
    const sanpham = {
      productname: this.ahihi.value.ten,
      productprice: this.ahihi.value.gia,
      typeproductid: this.ahihi.value.loai,
      productimage: this.hinh
    }
    this.productService.createProduct(sanpham).subscribe((data) => {
      this.getAllProducts();
      this.toastr.success('Thêm sản phẩm thành công');
    })
  }
  deleteProduct(id) {
    const data = {
      id: id
    }
    this.productService.deleteProduct(data).subscribe((data) => {
      this.getAllProducts();
      this.toastr.success('Xóa sản phẩm thành công');
    });
  }
  logout() {
    $('.ask').fadeIn(300);
    // if (confirm('Bạn có muốn đăng xuất không')) {
    //   this.toastr.success('Đăng xuất thành công');
    //   localStorage.clear();
    //   this.router.navigate(['/auth/login']);
    // }
  }
  yes() {
    this.toastr.success('Đăng xuất thành công');
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
  no() {
    $('.ask').fadeOut(300);
  }
}
