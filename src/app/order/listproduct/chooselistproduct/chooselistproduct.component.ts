import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/@core/http';
import { TypeProduct } from 'src/@core/models/type-product';
import { of, from } from 'rxjs';
import { Product } from 'src/@core/models/product';
import { importType } from '@angular/compiler/src/output/output_ast';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { ModalModule } from 'ngx-modal';
import { Table } from 'src/@core/models/table';
import { TableService } from 'src/@core/http/table/table.service';
import { BillService } from 'src/@core/http/bill/bill.service';

@Component({
  selector: 'app-chooselistproduct',
  templateUrl: './chooselistproduct.component.html',
  styleUrls: ['./chooselistproduct.component.scss']
})
export class ChooselistproductComponent implements OnInit {

  constructor(
    private productService: ProductService,
    private router: Router,
    private tableService: TableService,
    private billService: BillService) { }
  products: any[] = [];
  listType: any;
  itemtype: TypeProduct;
  idProductType: Number;
  listItemById: Product[];
  product: Product[];
  selectedProduct: Product;
  selectedTable: Table;
  ngOnInit() {
    console.log(this.product);
    
    // $('list').trigger('click');
    this.selectedTable = this.tableService.detailTable;
    this.productService.getTypeProduct().subscribe((data) => {
      // console.log(data);
      this.listType = data;
      // this.productService.productList = this.listItemById;
    });

    // $('.m_btn').click(function () {
    //   $('.pro').hide();
    //   $('.pro').fadeIn(500);
    // });


    // const addclass = 'color';
    // $('.m_btn').click(function (e) {
    //   e.preventDefault();
    //   $('.m_btn').removeClass(addclass);
    //   $(this).addClass(addclass);
    // });

    // ahihi
    this.productService.getProductsByIDtypeProduct(this.idProductType = 1).subscribe((data) => {
      this.listItemById = data as Product[];
      // console.log(this.listItemById);
      this.productService.productList = this.listItemById;
    });
    this.billService.productAdded$.subscribe(data => {
      this.products = data.products;
      // this.billTotal = data.billTotal;
      // console.log(data);
      // this.numProducts = data.products.reduce((acc, product) => {
      //   acc += product.quantity;
      //   return acc;
      // }, 0);
    });
  }
  select() {
    const addclass = 'color';
    $('.click').click(function (e) {
      e.preventDefault();
      $('.click').removeClass(addclass);
      $(this).addClass(addclass);
    });

    $('.click').click(function () {
      $('.pro').hide();
      $('.pro').fadeIn(500);
    });
  }
  selectedType(typ: TypeProduct) {
    this.itemtype = typ;
    this.idProductType = typ.TypeProductId;
    // console.log(this.idProductType);
    this.getById();
  }
  getById() {
    this.productService.getProductsByIDtypeProduct(this.idProductType).subscribe((data) => {
      this.listItemById = data as Product[];
      // console.log(this.listItemById);
      this.productService.productList = this.listItemById;
    });
  }
  onSelect(product: Product): void {
    this.selectedProduct = product;
    // console.log('Detail going:', this.selectedProduct);

    this.productService.detail = this.selectedProduct;

    this.router.navigate(['order/table/home/detail']);
  }
}
