import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooselistproductComponent } from './chooselistproduct.component';

describe('ChooselistproductComponent', () => {
  let component: ChooselistproductComponent;
  let fixture: ComponentFixture<ChooselistproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooselistproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooselistproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
