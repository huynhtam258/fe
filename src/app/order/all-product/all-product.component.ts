import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/@core/http';
import { ToastrService } from 'ngx-toastr';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from 'angularfire2/storage';
import { map } from 'rxjs/operators/map';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-product',
  templateUrl: './all-product.component.html',
  styleUrls: ['./all-product.component.scss']
})
export class AllProductComponent implements OnInit {
  ahihi: FormGroup;
  editForm:FormGroup
  hinh: String;
  listProduct: any = [];
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadState: Observable<string>;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;
  product;
  sanphamchinhsua:any
  constructor(private productService:ProductService, private toastr:ToastrService,private afStorage: AngularFireStorage,private router: Router) { }

  ngOnInit() {
    this.createForm();
    this.getAllProducts();
  }
  getAllProducts(){
    this.productService.getAllProducts().subscribe((data:any)=>{this.listProduct = data})
  }
  createForm() {
    this.ahihi = new FormGroup({
      ten: new FormControl(),
      gia: new FormControl(),
      loai: new FormControl()
    })
  }
  upload(event) {
    const id = Math.random().toString(36).substring(2);
    this.ref = this.afStorage.ref(id);
    this.task = this.ref.put(event.target.files[0]);
    this.uploadState = this.task.snapshotChanges().pipe(map(s => s.state));
    this.uploadProgress = this.task.percentageChanges();
    this.task.snapshotChanges().pipe(
      finalize(() => {
        this.ref.getDownloadURL().subscribe(url => {
          this.hinh = url
          console.log(url);
        });
      })
    ).subscribe();

  }
  add() {
    const sanpham = {
      productname: this.ahihi.value.ten,
      productprice: this.ahihi.value.gia,
      typeproductid: this.ahihi.value.loai,
      productimage: this.hinh
    }
    this.productService.createProduct(sanpham).subscribe((data) => {

    })
  }
  share(sanpham){
    this.product = sanpham
    console.log(this.product);
    
  }
  editProduct(){
    // this.ahihi = new FormGroup({
    //   ten: new FormControl({value:this.product.ProductName}),
    //   gia: new FormControl({value:this.product.ProductPrice}),
    //   loai: new FormControl({value:this.product.TypeProductId})
    // })
    this.productService.getProduct()
    console.log(this.product);
    const data={
      id:this.product.ProductId
    }
    const sanpham = {
      ProductName: this.ahihi.value.ten,
      ProductPrice: this.ahihi.value.gia,
      TypeProductId: this.ahihi.value.loai,
      ProductImage: this.hinh
    }
    this.productService.editProduct(data,sanpham).subscribe((data)=>{
    })
  }
  deleteProduct(id) {
    const data = {
      id: id
    }
    this.productService.deleteProduct(data).subscribe((data) => {
      this.getAllProducts();
      this.toastr.success('Xóa thành công');
    });
  }
  back(){
    this.router.navigate(['/order/table'])
  }
}
