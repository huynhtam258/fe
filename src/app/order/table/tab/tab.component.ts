import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Table } from 'src/@core/models/table';
import {  } from '@angular/animations';
import { from } from 'rxjs';
import { Product } from 'src/@core/models/product';
import { TableService } from 'src/@core/http/table/table.service';
import { BillService } from 'src/@core/http/bill/bill.service';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {
  @Input() Table;
  table: Table;
  @Input() nameTab: String;
  @Input() idTab: Number;
  @Input() statTab: String;
  @Input() nameCus: String;
  @Input() orderTab: {};
  @Input() Newname;

  giohang: any[];
  tongsanpham = 0;
  mangsoluong = [];
  soluong = 0;
  // productget: any[] = [];
  constructor(private tableService:TableService) { }
  quality = 1;
  
  ngOnInit() {
 
    this.tableService.TableId =this.idTab;
    if (localStorage.getItem(this.nameTab + '') != null) {
      this.demsoluong();
    }
  }
  demsoluong() {
    this.giohang = JSON.parse(localStorage.getItem(this.nameTab + ''));
    for (let i = 0; i < this.giohang.length; i++) {
      this.mangsoluong.push(this.giohang[i].quantity);
    }
    for (let i = 0; i < this.mangsoluong.length; i++) {
      this.tongsanpham += this.mangsoluong[i];
    }
    this.soluong = this.tongsanpham;
    // console.log('So luong', this.soluong);
  }
}
