import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { TableService } from 'src/@core/http/table/table.service';
import { Table } from 'src/@core/models/table';
import { ToastrService } from 'ngx-toastr';
import { TokenService } from 'src/@core/http/token/token.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { from } from 'rxjs';
import { ProductService } from 'src/@core/http';
// import {  } from '../../../../@core/http/table'
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  table = [];
  selectedTable: Table;
  products: any[] = [];
  item = {
    IdTable: '',
    NameTable: '',
    StatusTable: '',
    NameCus: '',
    Order: []
  }
  // tslint:disable-next-line:max-line-length
  constructor(private productService: ProductService, private spinner: NgxSpinnerService, private router: Router, public tableService: TableService, public toastr: ToastrService, public tokenService: TokenService) { }
  isGrid: Boolean = false;
  ngOnInit() {
    $('.ask').hide();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
    // console.log(this.tokenService.getToken());
    $('.grid').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });

    $('.list').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });
    // this.getTable();
    this.getTables();
  }
  getTables() {
    this.tableService.getTable().subscribe((dataTable: any) => {
      // console.log(dataTable);
      dataTable.map((dataItem) => {
        this.item.IdTable = dataItem.TableId;
        this.item.NameTable = dataItem.TableName;
        this.table.push({
          IdTable: dataItem.TableId,
          NameTable: dataItem.TableName,
          StatusTable: '',
          NameCus: '',
          Order: []
        })
        console.log(this.table);
        this.tableService.listTable = this.table;
      })



    })
  }
  showtable(){
    this.router.navigate(['/order/listtable']);
  }
  selectTable(table: Table): void {
    this.products = [];
    this.selectedTable = table;
    this.tableService.detailTable = this.selectedTable;
    if (localStorage.getItem(this.selectedTable.NameTable + '')) {
      this.router.navigate(['order/table/home']);
    } else {
      localStorage.setItem(this.selectedTable.NameTable + '', JSON.stringify(this.products));
      this.router.navigate(['order/table/home']);
    }


  }
  themban() {
    const data = { tablename: $('#nametable').val() }
    this.productService.createTable(data).subscribe((data) => {
      if(data){
        this.table = []
        this.getTables();
        // this.toastr.success("Tạo thành công")
      }else{
       
      }
      
    },(err)=>{ this.toastr.error('Xin kiểm tra lại')})
    // console.log($('#nametable').val());

  }
  thongke() {
    this.router.navigate(["/order/bill"])
  }
  getTable(): void {
    this.tableService.getProduct().subscribe((updateTable) => {
      this.table = updateTable;
      // console.log(this.table);

    });
  }
  logout() {
    $('.ask').fadeIn(300);
    // if (confirm('Bạn có muốn đăng xuất không')) {
    //   this.toastr.success('Đăng xuất thành công');
    //   localStorage.clear();
    //   this.router.navigate(['/auth/login']);
    // }
  }
  yes() {
    this.toastr.success('Đăng xuất thành công');
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
  no() {
    $('.ask').fadeOut(300);
  }
  // home() {
  //   this.router.navigate(['order/table/home']);
  // }
}


