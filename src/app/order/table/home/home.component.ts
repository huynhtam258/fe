import { Component, OnInit, Input, OnChanges, SimpleChange } from '@angular/core';
// import { Product } from '../../shared/models/product';
// import { dataProduct } from '../../shared/fake-data/dataProduct';
// import { Product } from '../../../shared/models/product';
import { ProductService } from '../../../../@core/http/product/product.service';
import { Subscriber } from 'rxjs';
import { Router } from '@angular/router';
import { Product } from 'src/@core/models/product';
import { TableService } from 'src/@core/http/table/table.service';
import { Table } from 'src/@core/models/table';

import { BillService } from 'src/@core/http/bill/bill.service';

import * as $ from 'jquery';
import { TokenService } from 'src/@core/http/token/token.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  value: Boolean = true;
  product: Product[];
  selectedProduct: Product;
  selectedTable: Table;
  tongsanpham = 0;
  mangsoluong = [];
  sodienthoai
  // Get number product in list
  products: any[] = [];
  numProducts = 0;
  billTotal = 0;
  listBill = [];
  giohang: any[];
  giohangtinhtien = [];
  sanphamdadat = 0;
  // tslint:disable-next-line:max-line-length
  constructor(private tableService: TableService, private productService: ProductService, private router: Router, private billService: BillService, private token: TokenService, private toastr: ToastrService) { }
  ngOnInit() {
    this.sodienthoai = this.billService.numberphone;
    this.selectedTable = this.tableService.detailTable;
    this.products = JSON.parse(localStorage.getItem(this.selectedTable.NameTable + ''));
    this.getProductFromService();
    // this.sanphamdadat = this.billService.sanphamdadat;
    if (this.productService.change === false) {
      return this.value = false;
    }
    this.demsoluong();
    // Get number product in list
    this.billService.productAdded$.subscribe(data => {
      this.products = data.products;
      this.billTotal = data.billTotal;
      this.numProducts = data.products.reduce((acc, product) => {
        acc += product.quantity;
        return acc;
      }, 0);
    });

    //
    // this.tableService.getNumpro(this.numProducts);
  }
  demsoluong() {
    this.giohang = JSON.parse(localStorage.getItem(this.selectedTable.NameTable + ''));
    for (let i = 0; i < this.giohang.length; i++) {
      this.mangsoluong.push(this.giohang[i].quantity);
    }
    for (let i = 0; i < this.mangsoluong.length; i++) {
      this.tongsanpham += this.mangsoluong[i];
    }
    this.numProducts = this.tongsanpham;
  }
  getProductFromService(): void {
    this.giohang = JSON.parse(localStorage.getItem(this.selectedTable.NameTable + ''));
    localStorage.setItem(this.selectedTable.NameTable + '', JSON.stringify(this.products));
    this.billService.getBill(JSON.parse(localStorage.getItem(this.selectedTable.NameTable + '')));
    this.tableService.detailTable.StatusTable = 'Ready';
    this.billService.nameTable = this.selectedTable.NameTable;
    // this.sanphamdadat = this.billService.sanphamdadat;
  }


  // hàm bắt sự kiện lấy dữ liệu detail sản phẩm
  onSelect(product: Product): void {
    this.selectedProduct = product;
    // console.log('Detail going:', this.selectedProduct);
    this.productService.detail = this.selectedProduct;
    this.router.navigate(['order/table/home/detail']);
  }

  change() {
    if (this.value.valueOf() === false) {
      this.value = true;
    } else if (this.value === true) {
      this.value = false;
    }
  }

  table() {
    this.billService.products = [];
    this.router.navigate(['order/table']);
  }
  orderdetail() {
    // console.log(this.products);
    this.billService.getBill(this.products);
    localStorage.removeItem(this.selectedTable.NameTable + '');
    localStorage.setItem(this.selectedTable.NameTable + '', JSON.stringify(this.products));
    this.router.navigate(['order/table/home/orderdetail']);
  }
}

