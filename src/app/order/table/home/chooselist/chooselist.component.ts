import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/@core/http';
import { TypeProduct } from 'src/@core/models/type-product';
import { of, from } from 'rxjs';
import { Product } from 'src/@core/models/product';
import { importType } from '@angular/compiler/src/output/output_ast';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { ModalModule } from 'ngx-modal';
import { Table } from 'src/@core/models/table';
import { TableService } from 'src/@core/http/table/table.service';
import { BillService } from 'src/@core/http/bill/bill.service';

@Component({
  selector: 'app-chooselist',
  templateUrl: './chooselist.component.html',
  styleUrls: ['./chooselist.component.scss']
})
export class ChooselistComponent implements OnInit {

  // tslint:disable-next-line:max-line-length
  constructor(private productService: ProductService, private router: Router, private tableService: TableService, private billService: BillService) { }
  products: any[] = [];
  listType: any;
  itemtype: TypeProduct;
  idProductType: Number;
  listItemById: Product[];
  product: Product[];
  selectedProduct: Product;
  selectedTable: Table;
  ngOnInit() {
    // this.select();
    $('.list').trigger('click');
    this.selectedTable = this.tableService.detailTable;
    this.productService.getTypeProduct().subscribe((data) => {
      // console.log(data);
      this.listType = data;
      // this.productService.productList = this.listItemById;
    });

    $('.m_btn').click(function () {
      $('.pro').hide();
      $('.pro').fadeIn(500);
    });

    // ahihi
    this.productService.getProductsByIDtypeProduct(this.idProductType = 1).subscribe((data) => {
      this.listItemById = data as Product[];
      // console.log(this.listItemById);
      this.productService.productList = this.listItemById;
    });
    this.billService.productAdded$.subscribe(data => {
      this.products = data.products;
      // this.billTotal = data.billTotal;
      // console.log(data);
      // this.numProducts = data.products.reduce((acc, product) => {
      //   acc += product.quantity;
      //   return acc;
      // }, 0);
    });
  }
  // select() {
  //   const addclass = 'color';
  //   $('.click').click(function (e) {
  //     e.preventDefault();
  //     $('.click').removeClass(addclass);
  //     $(this).addClass(addclass);
  //   });
  // }
  selectedType(typ: TypeProduct) {
    $('.click').click(function (e) {
      e.preventDefault();
      $('.click').removeClass(addclass);
      $(this).addClass(addclass);
    });
    this.itemtype = typ;
    this.idProductType = typ.TypeProductId;
    // console.log(this.idProductType);
    this.getById();
    if (this.products.length > 0) {
      localStorage.setItem(this.selectedTable.NameTable + '', JSON.stringify(this.products));
    }
    const addclass = 'color';
    // console.log('Hello', this.products);
  }
  getById() {
    this.productService.getProductsByIDtypeProduct(this.idProductType).subscribe((data) => {
      this.listItemById = data as Product[];
      // console.log(this.listItemById);
      this.productService.productList = this.listItemById;
    });
  }
  // getById(){
  //   this.productService.getProductsByIDtypeProduct(this.idProductType).pipe()
  // }

  onSelect(product: Product): void {
    this.selectedProduct = product;
    // console.log('Detail going:', this.selectedProduct);

    this.productService.detail = this.selectedProduct;

    this.router.navigate(['order/table/home/detail']);
  }
}
