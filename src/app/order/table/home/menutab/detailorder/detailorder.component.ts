import { Component, OnInit, Input, Output } from '@angular/core';
import { BillService } from 'src/@core/http/bill/bill.service';
import { Router } from '@angular/router';
import { Product } from 'src/@core/models/product';
import { Table } from 'src/@core/models/table';
import { TableService } from 'src/@core/http/table/table.service';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-detailorder',
  templateUrl: './detailorder.component.html',
  styleUrls: ['./detailorder.component.scss']
})
export class DetailorderComponent implements OnInit {
  listbill: any[] = [];
  selectedTable: Table;
  day = new Date();
  currentDay = `${this.day.getFullYear()}/${this.day.getMonth()+1}/${this.day.getDate()}`
  ahihi = {
    employeeid: 1,
    custormerphone: "",
    billcreatedate: this.currentDay,
    billpricetotal: this.billService.tongsotien,
    tableid:this.tableService.detailTable.IdTable,
    areaId: "1"
  }
  customer = {
    customerphone: this.billService.numberphone
  }
  bill = {
    BillOutId: 0,
    QuantityProduct: 0,
    BillPriceTotal: 0,
    ProductId: 1
  };
  billdetail = {
    billoutid: 0,
    productname:'',
    quantityproduct: 0,
    productid: 0,
    productprice:0
  }
  @Input() product: Product;
  // tslint:disable-next-line:max-line-length
  constructor(private billService: BillService, private router: Router, private tableService: TableService, private toastr: ToastrService) { }

  ngOnInit() {
    if(!this.billService.numberphone){
      this.customer.customerphone = "0123456789";
    }
    this.selectedTable = this.tableService.detailTable;
    this.listbill = JSON.parse(localStorage.getItem(this.selectedTable.NameTable + ''));
    console.log(this.listbill);
  }
  home() {
    this.router.navigate(['/order/table/home']);
  }
  thanhtoan() {
    this.bill.QuantityProduct = this.billService.sanphamdadat;
    this.bill.BillPriceTotal = this.billService.tongsotien;
  if(this.billService.numberphone != null){
    this.billService.postCustomer(this.customer).subscribe((customerData: any) => {
      if (customerData) {
        this.ahihi.custormerphone = customerData.CustomerPhone;
        this.billService.postBill(this.ahihi).subscribe((data: any) => {
          this.listbill.forEach((e) => {
            this.billdetail.billoutid = data.BillOutId;
            this.billdetail.productid = e.product.ProductId;
            this.billdetail.productname = e.product.ProductName;
            this.billdetail.productprice = e.product.ProductPrice;
            this.billdetail.quantityproduct = e.quantity;
            this.billService.postDetailBill(this.billdetail).subscribe((data) => {
              this.toastr.info('Thanh toán thành công');
              localStorage.removeItem(this.selectedTable.NameTable + '');
              this.billService.products = [];
              this.router.navigate(['/order/table']);
            })
          })
        })
      }
    })
  }
  else{
    this.toastr.error('thanh toán không thành công');
  }

    // this.billService.postDetailBill(this.bill).subscribe((data) => {
    //   this.toastr.info('Thanh toán thành công');
    //   localStorage.removeItem(this.selectedTable.NameTable + '');
    //   this.billService.products = [];
    //   this.router.navigate(['/order/table']);
    // }, (err) => {
    //   this.toastr.error('Lỗi thanh toán');
    // });
  }
}
