import { Component, OnInit, Input } from '@angular/core';

import { Router } from '@angular/router';
import { Product } from 'src/@core/models/product';

import { BillService } from 'src/@core/http/bill/bill.service';
// import { count } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { TableService } from 'src/@core/http/table/table.service';
import { Table } from 'src/@core/models/table';
import { element } from '@angular/core/src/render3/instructions';

// import { Product } from '../../../../shared/models/product';

@Component({
  selector: 'app-menutab',
  templateUrl: './menutab.component.html',
  styleUrls: ['./menutab.component.scss']
})
export class MenutabComponent implements OnInit {
  // exam = [1, 2, 3, 4];
  @Input() product: Product;
  @Input() nameProduct: string;
  @Input() priceProduct: number;
  @Input() idProduct: any;
  @Input() imageProduct: String;
  selectedProduct: Product;
  count = 0;
  gia = 0;
  danhsachmonan = [];
  mang: any[];
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private billService: BillService, private toastr: ToastrService, private tableService: TableService) { }
  selectedTable: Table;
  ngOnInit() {
    this.selectedTable = this.tableService.detailTable;
    // this.mang = JSON.parse(localStorage.getItem(this.selectedTable.NameTable + ''));
    // this.count = 0;
    if (JSON.parse(localStorage.getItem(this.selectedTable.NameTable + '')).length > 0) {
      this.billService.products = JSON.parse(localStorage.getItem(this.selectedTable.NameTable + ''));
    }
    // this.billService.danhsachmonan(this.danhsachmonan);
    this.danhsachmonan = JSON.parse(localStorage.getItem(this.selectedTable.NameTable + ''));
    if (this.danhsachmonan == null) {
      this.count = 0;
    } else {
      for (let i = 0; i < this.danhsachmonan.length; i++) {
        if (this.danhsachmonan[i].quantity > 0) {
          if (this.danhsachmonan[i].product.ProductName === this.nameProduct) {
            this.count = this.danhsachmonan[i].quantity;
          }
        }
      }
    }


  }

  onAddToBill() {
    this.billService.addProductToBill(this.product, this.selectedTable.IdTable);
    // console.log('Add Product:', this.product);
    // localStorage.setItem(this.selectedTable.NameTable + '', this.product + '');
    this.count++;
  }

  onRemoveFromBill() {
    this.billService.removeProductFromBill(this.product);
    if (this.count <= 0) {
      this.toastr.error('Chưa có sản phẩm má ơi');
    }
    if (this.count > 0) {
      this.count--;
    }
  }
}
