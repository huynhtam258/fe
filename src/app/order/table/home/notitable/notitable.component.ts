import { Component, OnInit } from '@angular/core';
import { TableService } from 'src/@core/http/table/table.service';
import { Table } from 'src/@core/models/table';
import * as $ from 'jquery';
import { BillService } from 'src/@core/http/bill/bill.service';

@Component({
  selector: 'app-notitable',
  templateUrl: './notitable.component.html',
  styleUrls: ['./notitable.component.scss']
})
export class NotitableComponent implements OnInit {
  selectedTable: Table;
  nameCus: String;
  constructor(private billService:BillService, private tableService: TableService) { }

  ngOnInit() {
    this.nameCus = this.tableService.detailTable.NameCus;
    this.selectedTable = this.tableService.detailTable;
  }

  change() {
    this.tableService.detailTable.NameCus = $('#name').val();
    console.log($('#name').val());
    this.billService.numberphone = $('#name').val();
    // localStorage.setItem(this.selectedTable.NameTable + '', JSON.stringify(this.tableService.detailTable.NameCus));
  }
}
