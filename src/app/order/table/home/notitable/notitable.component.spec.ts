import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotitableComponent } from './notitable.component';

describe('NotitableComponent', () => {
  let component: NotitableComponent;
  let fixture: ComponentFixture<NotitableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotitableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotitableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
