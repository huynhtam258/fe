import { Component, OnInit } from '@angular/core';
import { PARAMETERS } from '@angular/core/src/util/decorators';
import { ActivatedRoute, Router } from '@angular/router';
import { BillService } from 'src/@core/http/bill/bill.service';
// import { privateDecrypt } from 'crypto';

@Component({
  selector: 'app-showbill',
  templateUrl: './showbill.component.html',
  styleUrls: ['./showbill.component.scss']
})
export class ShowbillComponent implements OnInit {
  idParam ={
    id:0
  }
  day = new Date()
  showBill = []
  constructor(private activatedRoute: ActivatedRoute,private billService:BillService, private router:Router) { }

  ngOnInit() {
    console.log(this.day.getDate());
    console.log(this.day.getMonth()+1)
    console.log(this.day.getFullYear())
   this.activatedRoute.paramMap.subscribe((params) => {
    // this.id = params.get('id');
    this.idParam.id = JSON.parse(params.get('id'));
    this.billService.getDetailBillByIdBill(this.idParam).subscribe((data:any)=>{
      this.showBill =data;
      console.log(this.showBill);
      
    })
   });
  
   
  }
  logout(){
    this.router.navigate(['/order/table'])
  }
  back(){
    this.router.navigate(['/order/bill'])
  }
}
