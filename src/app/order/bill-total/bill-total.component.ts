import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import * as $ from 'jquery';
import { BillService } from 'src/@core/http/bill/bill.service';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-bill-total',
  templateUrl: './bill-total.component.html',
  styleUrls: ['./bill-total.component.scss']
})
export class BillTotalComponent implements OnInit {
  
  day = new Date();
  dataInput:FormGroup
  getday = `${this.day.getFullYear()}-${this.day.getMonth()+1}-${this.day.getDate()}`;
  dataAday = {
    id: 1,
    day: this.day.getDate(),
    mon: (this.day.getMonth()+1),
    year: 2019
  }
  totalMoney:Number;
  listBill:any = [];
  constructor(private router: Router, private spinner: NgxSpinnerService, private billService: BillService, private toastr:ToastrService) { }

  ngOnInit() {
    $('.ask').hide();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
    // console.log(this.tokenService.getToken());
    $('.grid').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });

    $('.list').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });
    console.log(this.getday);
    this.dataInput =new FormGroup ({
      getDate:new FormControl(),
      getMonth:new FormControl(),
      getYear:new FormControl()
    })
    $('.ask').hide();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
    // console.log(this.tokenService.getToken());
    $('.grid').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });

    $('.list').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });
   
   this.getBill()
    this.getMoney()
  }
  getBill(){
    this.billService.getBillADay(this.dataAday).subscribe((data) => {
      this.listBill = data;
      if(data){
       
      }else{
        this.toastr.error("Kiểm tra lại ngày tháng năm")
      }
     
    })
  }
  getMoney(){
    this.billService.getTotalMoneyADay(this.dataAday).subscribe((dataADay:Number)=>{
      // console.log(dataADay);
      this.totalMoney = dataADay
      
    })
  }
  showTime(){
    this.dataAday.day = this.dataInput.value.getDate;
    this.dataAday.mon = this.dataInput.value.getMonth;
    this.dataAday.year = this.dataInput.value.getYear;
    this.getBill();
    this.getMoney();
  }
  setDay(){
    this.billService.getBillADay(this.dataAday).subscribe((data) => {
      console.log(data);
      this.listBill = data;
    }) 
  }
  back() {
    this.router.navigate(["/order/table"])
  }
  showBill(id){
    this.router.navigate([`/order/showBill/${id}`])
  }
  logout() {
    $('.ask').fadeIn(300);
    // if (confirm('Bạn có muốn đăng xuất không')) {
    //   this.toastr.success('Đăng xuất thành công');
    //   localStorage.clear();
    //   this.router.navigate(['/auth/login']);
    // }
  }
  yes() {
    this.toastr.success('Đăng xuất thành công');
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
  no() {
    $('.ask').fadeOut(300);
  }
}
