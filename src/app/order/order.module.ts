import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { OrderComponent } from './order.component';
import { HomeComponent } from './table/home/home.component';
import { ModalModule } from 'ngx-modal';
import { TableComponent } from './table/table.component';
import { TabComponent } from './table/tab/tab.component';
import { MenutabComponent } from './table/home/menutab/menutab.component';
import { NotitableComponent } from './table/home/notitable/notitable.component';
import { ChooselistComponent } from './table/home/chooselist/chooselist.component';

import { from } from 'rxjs';
// import { BillComponent } from './table/home/bill/bill.component';
import { DetailComponent } from '../order/listproduct/detail/detail.component';
// import { ReverseStr } from '../shared/pipe/reverseStr.pipe';
import { SharedModule } from 'src/@shared/shared.module';
import { BillComponent } from 'src/@shared/components/bill/bill.component';
// import { ReverseStr } from 'src/@shared/pipe/reverseStr.pipe';
import { NgxStepperModule } from 'ngx-stepper';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard } from 'src/@core/http/config/guard';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DetailorderComponent } from './table/home/menutab/detailorder/detailorder.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptor } from 'src/@core/http/config/http-interceptor';
import { ListproductComponent } from './listproduct/listproduct.component';
import { ChooselistproductComponent } from './listproduct/chooselistproduct/chooselistproduct.component';
import { MenutablistComponent } from './listproduct/menutablist/menutablist.component';
import { BillTotalComponent } from './bill-total/bill-total.component';
import { ShowbillComponent } from './showbill/showbill.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AllProductComponent } from './all-product/all-product.component';
import { AdminGuard } from 'src/@core/http/config/admin.guard';
import { ListtableComponent } from './listtable/listtable.component';
// import { AngularFireModule } from 'angularfire2';
// import { AngularFireStorageModule } from 'angularfire2/storage';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
const routes: Routes = [
  { path: '', component: OrderComponent },
  { path: 'table', component: TableComponent },
  { path: 'table/home', component: HomeComponent },
  { path: 'productlist/detail', component: DetailComponent },
  { path: 'table/home/orderdetail', component: DetailorderComponent },
  { path: 'productlist', component: ListproductComponent },
  { path: 'bill',component:BillTotalComponent},
  { path: 'showBill/:id',component:ShowbillComponent},
  { path: 'showProduct',component:AllProductComponent, canActivate:[AdminGuard]},
  { path: 'listtable',component:ListtableComponent, canActivate:[AdminGuard]}
];
@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    RouterModule.forChild(routes),
    ModalModule,
    // AngularFireModule.initializeApp({
    //   apiKey: "AIzaSyA0yf6M7UW1T_5dWTPohf7o-ObD4SHGLys",
    //   authDomain: "baocao-f4350.firebaseapp.com",
    //   databaseURL: "https://baocao-f4350.firebaseio.com",
    //   projectId: "baocao-f4350",
    //   storageBucket: "",
    //   messagingSenderId: "35455263198",
    //   appId: "1:35455263198:web:a4e6ad79b0e549cb"
    // }),
    // AngularFireStorageModule,
    NgxStepperModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 500,
      toastClass: 'toast-top-right',
      preventDuplicates: false
    }),
    NgxSpinnerModule
    // BrowserAnimationsModule
  ],
  providers: [
    AdminGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  declarations: [
    // ReverseStr,
    OrderComponent,
    HomeComponent,
    TableComponent,
    TabComponent,
    MenutabComponent,
    NotitableComponent,
    ChooselistComponent,
    BillComponent,
    DetailComponent,
    DetailorderComponent,
    ListproductComponent,
    ChooselistproductComponent,
    ListtableComponent,
    MenutablistComponent,
    BillTotalComponent,
    ShowbillComponent,
    AllProductComponent
  ]
})
export class OrderModule { }
