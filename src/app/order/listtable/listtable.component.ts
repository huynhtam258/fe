import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import * as $ from 'jquery';
import { NgxSpinnerService } from 'ngx-spinner';
import { TableService } from 'src/@core/http/table/table.service';
import { ProductService } from 'src/@core/http';
@Component({
  selector: 'app-listtable',
  templateUrl: './listtable.component.html',
  styleUrls: ['./listtable.component.scss']
})
export class ListtableComponent implements OnInit {
  danhsachban = [];
  ban
  constructor(private productService: ProductService, public toastr: ToastrService, private router: Router, private spinner: NgxSpinnerService, public tableService: TableService) { }

  ngOnInit() {
    $('.ask').hide();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
    // console.log(this.tokenService.getToken());
    $('.grid').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });

    $('.list').click(function () {
      $('.tab').hide();
      $('.tab').fadeIn(500);
    });
    this.getTable();
  }
  getTable() {
    this.tableService.getTable().subscribe((datalist:any)=>{
      console.log(datalist);
      this.danhsachban = datalist
      
    })
    // this.tableService.listTable.length > 0 ? this.danhsachban = this.tableService.listTable : this.router.navigate(['/order/table'])
  }
  deleteTable(id) {
    const data = {
      id: id
    }
    this.productService.deleteTable(data).subscribe((data) => {
      // this.getTable();
      this.danhsachban = this.tableService.listTable;
      this.toastr.success('Xóa thành công');
    });
  }
  share(table) {
    this.ban = table
   
  }
  editTable(){
    const dataid = {
      tableid: this.ban.TableId
    }
    console.log($('#nametable').val());

    const data = {
      TableName: $('#nametable').val()
    }
    this.productService.editTable(dataid, data).subscribe((data) => {
      this.getTable();
      this.toastr.success('Chỉnh sửa thành công');
      $('#nametable').val('')
    })
  }
  backTable(){
    this.router.navigate(['/order/table'])
  }
  logout() {
    $('.ask').fadeIn(300);
    // if (confirm('Bạn có muốn đăng xuất không')) {
    //   this.toastr.success('Đăng xuất thành công');
    //   localStorage.clear();
    //   this.router.navigate(['/auth/login']);
    // }
  }
  yes() {
    this.toastr.success('Đăng xuất thành công');
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
  no() {
    $('.ask').fadeOut(300);
  }
  // home() {
  //   this.router.navigate(['order/table/home']);
  // }
}
