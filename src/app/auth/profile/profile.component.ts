import { Component, OnInit, Input } from '@angular/core';
import { NgForm, Validators, FormBuilder } from '@angular/forms';
import * as $ from 'jquery';
import { LoginService } from 'src/@core/http/login/login.service';
import { User } from 'src/@core/models/user';
import { ToastrService } from 'ngx-toastr';
import { AngularFireStorageReference, AngularFireUploadTask, AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators/map';
import { finalize } from 'rxjs/operators';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadState: Observable<string>;
  uploadProgress: Observable<number>;
  downloadURL: Observable<string>;
  // tslint:disable-next-line:max-line-length
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  constructor(public loginService: LoginService, private fb: FormBuilder, private toastr: ToastrService, private afStorage: AngularFireStorage) { }
  username: String = localStorage.getItem('user');
  serverErrorMessages: String;
  dataUser: User;
  hinh;
  checkImg=""
  dataUserObj: any = {};
  EditForm = this.fb.group({
    UserName: ['', Validators.required],
    EmployeePhone: ['', Validators.required],
    EmployeeEmail: ['', Validators.required],
    EmployeeAvatar: ['', Validators.required],
    EmployeeName: ['', Validators.required]
  });
  newuser: Object = this.EditForm.value;
  ngOnInit() {
    // $('input').trigger('click');
   
    
    this.loadUI();
    $('input').attr('disabled', 'disabled');
    // tslint:disable-next-line:prefer-const
    let btn_save = $('.save-profile'),
      // tslint:disable-next-line:prefer-const
      btn_edit = $('.edit-profile'),
      // tslint:disable-next-line:prefer-const
      img_object = $('.img-object'),
      // tslint:disable-next-line:prefer-const
      overlay = $('.media-overlay'),
      // tslint:disable-next-line:prefer-const
      media_input = $('#media-input');

    btn_save.hide(0);
    overlay.hide(0);

    btn_edit.on('click', function () {
      $(this).hide(0);
      overlay.fadeIn(300);
      btn_save.fadeIn(300);
    });
    btn_save.on('click', function () {
      $(this).hide(0);
      overlay.fadeOut(300);
      btn_edit.fadeIn(300);
    });

    media_input.change(function () {
      if (this.files && this.files[0]) {
        const reader = new FileReader();

        reader.onload = function (e) {
          img_object.attr('src', e.target);
          // img_object.attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
      }
    });

    this.loginService.getUserByUserName(this.username).subscribe((data) => {
      this.dataUser = data as User;
      // this.loginService.User = this.dataUser;
      // console.log('First', this.dataUser);
      this.dataUserObj = this.dataUser[0];
    this.checkImg = this.dataUser.Avartar;
      // console.log('User get', this.dataUserObj);
    });
  }

  onSubmit(form: NgForm) {
    // console.log('Form', form.value);
    // console.log('Edit', this.EditForm);
    if (this.hinh != "") {
      form.value.EmployeeAvatar = this.checkImg;
    }



    const { UserName, EmployeePhone, EmployeeEmail, EmployeeName, EmployeeAvatar } = this.EditForm.value;
    // this.loginService.editUser(this.username, this.EditForm.value).subscribe((data: any) => {
    this.loginService.editUser(1, form.value).subscribe((data: any) => {
      // console.log({ UserName, EmployeePhone, EmployeeEmail, EmployeeName });
      // console.log(data);
      this.toastr.success('Hoàn tất.');
      console.log(form.value);

      localStorage.removeItem('user');
      localStorage.setItem('user', $('#username').val());
    }, (err) => {
      console.log(err);
    }
    );
  }
  upload(event) {
    const id = Math.random().toString(36).substring(2);
    this.ref = this.afStorage.ref(id);
    this.task = this.ref.put(event.target.files[0]);
    this.uploadState = this.task.snapshotChanges().pipe(map(s => s.state));
    this.uploadProgress = this.task.percentageChanges();
    this.task.snapshotChanges().pipe(
      finalize(() => {
        this.ref.getDownloadURL().subscribe(url => {
          this.hinh = url
          // console.log(this.EditForm.value.EmployeeAvatar);
        });
      })
    ).subscribe();

  }
  edit() {
    $('input').removeAttr('disabled');
  }

  done() {
    $('input').attr('disabled', 'disabled');
  }

  loadUI() {
    // var document
    // var window
    $(document).ready(function () {

      $('input').blur(function () {
        const $this = $(this);
        if ($this.val()) {
          $this.addClass('used');
        } else {
          $this.removeClass('used');
          $('label').removeClass('lbl');
        }
      });
      const $ripples = $('.ripples');
      $ripples.on('click.Ripples', function (e) {
        const $this = $(this);
        const $offset = $this.parent().offset();
        const $circle = $this.find('.ripplesCircle');
        const x = e.pageX - $offset.left;
        const y = e.pageY - $offset.top;
        $circle.css({
          top: y + 'px',
          left: x + 'px'
        });
        $this.addClass('is-active');
      });
      $ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function (e) {
        $(this).removeClass('is-active');
      });
    });
  }

}
