import { ElementRef, Directive, Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { LoginService } from '../../../@core/http/login/login.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { from } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Interceptor } from 'src/@core/http/config/http-interceptor';
import { NgxSpinnerService } from 'ngx-spinner';
// import { $ } from 'protractor';
import * as $ from 'jquery';

// import { request } from 'http';
// import { LoginService } from '../../shared/services/login.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  LoginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });
  // tslint:disable-next-line:max-line-length
  constructor(private spinner: NgxSpinnerService, private router: Router, private fb: FormBuilder, private loginService: LoginService, private toastr: ToastrService) { }

  ngOnInit() {
    $('.divloader').fadeIn(500);
    $('.login').hide();
    setTimeout(() => {
      $('.divloader').fadeOut(500);
      $('.login').show();
    }, 2000);
  }
  login() {
    const { username, password } = this.LoginForm.value;
    // console.log(username , password);
    this.loginService.loginEmployee({ username, password }).subscribe((data: any) => {
      localStorage.setItem('x', data);
      // console.log(data);
      // localStorage.setItem('roleId',data.RoleId)
      this.loginService.getUserByUserName(this.LoginForm.value.username).subscribe((data:any)=>{
        localStorage.setItem('roleId',data[0].RoleId)
      })
      this.toastr.success('Đăng nhập thành công');
      this.order();
    }, (err: HttpErrorResponse) => {
      if (err != null) {
        console.log(err);
        this.toastr.error('Thông tin tài khoản không đúng.');
      }
    });
    localStorage.removeItem('user');
    localStorage.setItem('user', $('#username').val());
  }
  order() {
    this.router.navigate(['order/table']);
  }
}
