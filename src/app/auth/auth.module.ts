import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule, FormBuilder, Validators } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from 'src/@shared/shared.module';
import { LoginService } from 'src/@core/http';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard } from 'src/@core/http/config/guard';
import { Interceptor } from 'src/@core/http/config/http-interceptor';
// import {  } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Profile } from 'selenium-webdriver/firefox';
import { ProfileComponent } from './profile/profile.component';

const router: Routes = [
  { path: '', component: AuthComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent }
];
@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(router),
    HttpClientModule,
    SharedModule,
    NgxSpinnerModule,
    ToastrModule.forRoot()
  ],
  declarations: [AuthComponent, LoginComponent, ProfileComponent],
  providers: [LoginService, AuthGuard],
})
export class AuthModule { }
