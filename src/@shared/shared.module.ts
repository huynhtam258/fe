import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { BottomDetailComponent } from './bottom-detail/bottom-detail.component';
// import { ReverseStr } from './pipe/reverseStr.pipe';
import { BottomDetailComponent } from './components/bottom-detail/bottom-detail.component';
import { NameDirective } from './directives/directives';

@NgModule({
    declarations: [
        BottomDetailComponent,
        NameDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        CommonModule,
        BottomDetailComponent,
        NameDirective
        // ReverseStr
    ],
    providers: [],
})
export class SharedModule {}
