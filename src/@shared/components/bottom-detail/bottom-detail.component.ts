import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

import { Router } from '@angular/router';

@Component({
  selector: 'app-bottom-detail',
  templateUrl: './bottom-detail.component.html',
  styleUrls: ['./bottom-detail.component.scss']
})
export class BottomDetailComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  const addclass = 'color';
  $('.it').click(function (e) {
    e.preventDefault();
    $('.it').removeClass(addclass);
    $(this).addClass(addclass);
  });

  // const addclass1 = 'color2';
  // $('.i').click(function (e) {
  //   e.preventDefault();
  //   $('.i').removeClass(addclass1);
  //   $(this).addClass(addclass1);
  // });
  // $('i').click(function (e) {
  //   e.preventDefault();
  //   $('i').removecss('color', '#422912');
  //   $('i').removecss('color', '#422912');
  //   $(this).css('color', '#422912');
  // });
  }
  user() {
    this.router.navigate(['auth/profile']);
  }
  table() {
    this.router.navigate(['order/table']);
  }
  productlist() {
    this.router.navigate(['order/productlist']);
  }
  

}
