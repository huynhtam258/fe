import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { ProductService } from 'src/@core/http';

import { BillService } from 'src/@core/http/bill/bill.service';
import { Table } from 'src/@core/models/table';
import { TableService } from 'src/@core/http/table/table.service';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit, OnChanges {

  // products = [];
  numProducts = 0;
  billTotal = 0;
  selectedTable: Table;
  value: Boolean = false;
  giahang = [];
  soluongsanpham = [];
  tongtien = 0;
  soluong = 0;
  @Input() products: any;


  constructor(public productService: ProductService, private billService: BillService, private tableService: TableService) { }

  ngOnInit() {


  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.products) {
      // this.products = JSON.parse(localStorage.getItem(this.selectedTable.NameTable + ''));
      if (this.giahang.length > 0) {
        this.giahang = [];
      }
      if (this.soluongsanpham.length > 0) {
        this.soluongsanpham = [];
      }
      this.products.forEach(element => {
        this.giahang.push(element.quantity * element.product.ProductPrice);
        this.soluongsanpham.push(element.quantity);
      });
      // console.log(this.giahang);
      this.tongtien = 0;
      this.soluong = 0;
      for (let i = 0; i < this.giahang.length; i++) {
        this.tongtien += this.giahang[i];
      }
      for (let i = 0; i < this.soluongsanpham.length; i++) {
        this.soluong += this.soluongsanpham[i];
      }
      // console.log(this.soluong);
      // console.log(this.tongtien);
      this.billService.getSoluong(this.soluong, this.tongtien);

    }
  }
}
