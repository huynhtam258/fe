import { Directive, ElementRef } from '@angular/core';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[directive]',
})
export class NameDirective {
    constructor(ef: ElementRef) {
        ef.nativeElement.style.backgroundColor = 'red';
    }
}
