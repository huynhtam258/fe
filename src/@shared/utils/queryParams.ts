export const queryParams = (request: object): string => {
    const result = [];
    Object.keys(request).map((key) => {
        result.push(`${key}=${request[key]}`);
    });
    return result.join('&');
};
