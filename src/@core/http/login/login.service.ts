import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { from, Observable, of } from 'rxjs';
import { queryParams } from 'src/@shared/utils/queryParams';
import { API } from '../config/api';
import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
import { TokenService } from '../token/token.service';
import { RequestMethod, RequestOptions, } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  userlist: any;
  readonly rootURL = 'http://localhost:57833';
  constructor(private http: HttpClient, private tokenService: TokenService) {
  }
  loginEmployee(request): Observable<any> {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.post(this.rootURL + API.AUTH.LOGIN(queryParams(request)), { headers: reqHeader });
    // return this.http.post(this.rootURL + '/api/employees/Login?',  { headers: reqHeader });
  }
  getUserByUserName(username) {
    return this.http.get(this.rootURL + API.AUTH.GET_BY_USER(username));
  }
  getUser() {
    return of(this.userlist);
  }
  editUser(id, request) {
    const reqHeader = new HttpHeaders().append('Content-Type' , 'application/json');
    return this.http.put(this.rootURL + API.AUTH.PUT_BY_USER(id), request, { headers: reqHeader });
  }
}
