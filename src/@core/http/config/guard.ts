import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }
    canActivate() {
        const checkToken = localStorage.getItem('x');
        // return checkToken ? true : false;
        console.log(checkToken);
        if (checkToken != null) {
            return true;
        } else {
            this.router.navigate(['/auth/login']);
        }
    }

}
