import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
// import { do } from 'rxjs/operators';
import 'rxjs/add/operator/do';
@Injectable()
export class Interceptor implements HttpInterceptor {
    constructor(private router: Router) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.headers.get('No-Auth') === 'True') {
            return next.handle(req.clone());
        }
        if (localStorage.getItem('x') != null) {
            const clonedreq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + localStorage.getItem('x'))
            });
            return next.handle(clonedreq).do(
                    succ => {},
                    err => {
                        if (err.status === 401) {
                            this.router.navigateByUrl('/auth/login');
                        }
                    }
                );
        } else {
            this.router.navigateByUrl('/auth/login');
        }

    }
}
