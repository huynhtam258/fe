export const API = {
    AUTH: {
        LOGIN: (user) => `/api/employees/Login?${user}`,
        GET_BY_USER: (username: string) => `/api/employees/GetEmployee?username=${username}`,
        PUT_BY_USER: (id: Number) => `/api/employees/PutEmployee?EmmployeeID=${id}`
    },
    PRODUCT: {
        GET_PRODUCT: `/api/Products/GetProducts`,
        // GET_PRODUCT_BY_ID:(id)=>`/api/Products/`s
        DELETE_PRODUCT_BY_ID:(id)=>`/api/Products/DeleteProduct?${id}`,
        GET_BY_ID: (id: string) => `/api/Products/GetProductsByIdTypeProduct?id=${id}`,
        CREATE_PRODUCT: (data)=> `/api/Products/PostProduct?${data}`,
        PUT_BY_PRODUCT: (id) => `/api/Products/PutProduct?${id}`
   
    },
    TABLE:{
        GET_TABLES:`/api/Tables/GetTypeProducts`,
        CREATE_TABLE:(data)=>`/api/Tables/PostTable?${data}`,
        DELETE_TABLE_BY_ID:(id)=>`/api/Tables/DeleteTable?${id}`,
        PUT_BY_TABLE: (id) => `/api/Tables/PutTable?${id}`
    },
    TYPEPRODUCT: {
        GET_TYPEPRODUCT: '/api/TypeProducts/GetTypeProducts'
    },
    BILL: {
        GET_TOTAL_MONEY_A_DAY:(data) => `/api/BillOuts/GetTotalMoneyADay?${data}`,
        GET_DETAIL_BY_DAY: (data) => `/api/BillOuts/GetBillADay?${data}`,
        POST_BILL: (bill) => `/api/BillOuts/PostBillOut?${bill}`
    },
    DETAILBILL: {
        GET_DETAIL_BY_ID_BILL: (id) => `/api/BillOuts/GetBillDetailByIdBill?${id}`,
        POST_BILL: (detailbill) => `/api/BillOutDetails/PostBillOutDetail?${detailbill}`
    },
    CUSTOMER: {
        POST_CUSTOMER: (customer) => `/api/Customer/PostCustomer?${customer}`
    }

};
