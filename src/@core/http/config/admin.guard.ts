import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AdminGuard implements CanActivate {
    constructor(private router: Router) { }
    canActivate() {
        const checkAdmin = localStorage.getItem('roleId');
        if (checkAdmin == '1') {
            return true;
        } else {
            this.router.navigate(['/order/table']);
        }
    }

}
