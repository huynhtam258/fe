import { Injectable } from '@angular/core';
import { dataProduct } from '../../../app/shared/fake-data/dataProduct';
import { API } from '../config/api';
import { Subject, from } from 'rxjs';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TypeProduct } from 'src/@core/models/type-product';
import { Product } from 'src/@core/models/product';
import { TokenService } from '../token/token.service';
import { queryParams } from 'src/@shared/utils/queryParams';
// import * as AWS from 'aws-sdk/global';
// import { S3 } from 'aws-sdk/clients/all';
// import * as S3 from 'aws-sdk/clients/s3';
// declare const Buffer
@Injectable({
  providedIn: 'root'
})

export class ProductService {
  formData: TypeProduct;
  public detail: Product;
  readonly rootULR = 'http://localhost:57833';
  typeProduct: TypeProduct[];
  productList: any;
  
  idType: Number;
  FOLDER = 'jsa-s3/';
  readonly rootURL = 'http://localhost:57833';
  change: Boolean = true;
  constructor(public http: HttpClient, private tokenService: TokenService) { }
  private token = this.tokenService.getToken();
  getProductsByIDtypeProduct(id) {
    return this.http.get(this.rootULR + API.PRODUCT.GET_BY_ID(id));
  }
  getTypeProduct() {
    return this.http.get(this.rootULR + API.TYPEPRODUCT.GET_TYPEPRODUCT);
  }
  /* #region  Observable*/
  // Xử lí bất đồng bộ với Observable
  // Observable có thể hiểu là một đối tượng được theo dõi liên tục
  /* #endregion */
  getAllProducts() {
    return this.http.get(this.rootULR + '/api/Products/GetProducts');
  }
  getProduct() {
    return of(this.productList);
    // this.getProductsByIDtypeProduct().subscribe((data) => {
    //   return data;
    // })
  }
  editProduct(id,data){
    const reqHeader = new HttpHeaders().append('Content-Type' , 'application/json');
    return this.http.put(this.rootURL + API.PRODUCT.PUT_BY_PRODUCT(queryParams(id)), data, { headers: reqHeader });
  }
  editTable(id,data){
    const reqHeader = new HttpHeaders().append('Content-Type' , 'application/json');
    return this.http.put(this.rootURL + API.TABLE.PUT_BY_TABLE(queryParams(id)), data, { headers: reqHeader });
  }
  deleteProduct(id){
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.delete(this.rootULR + API.PRODUCT.DELETE_PRODUCT_BY_ID(queryParams(id)), { headers: reqHeader });
  }
  deleteTable(id){
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.delete(this.rootULR + API.TABLE.DELETE_TABLE_BY_ID(queryParams(id)), { headers: reqHeader });
  }
  createProduct(data) {
    const reqHeader = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.rootULR + API.PRODUCT.CREATE_PRODUCT(queryParams(data)), { headers: reqHeader });
  }
  createTable(data) {
    const reqHeader = new HttpHeaders().append('Content-Type', 'application/json');
    return this.http.post(this.rootULR + API.TABLE.CREATE_TABLE(queryParams(data)), { headers: reqHeader });
  }
  getDetail(deltailProduct) {
    this.detail = deltailProduct;
  }
}
