import { Injectable } from '@angular/core';
import { dataTable } from '../../../app/shared/fake-data/dataTable';
import { Table } from 'src/@core/models/table';
import { Observable, of, Subject, BehaviorSubject } from 'rxjs';

import { API } from '../config/api';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { queryParams } from 'src/@shared/utils/queryParams';
@Injectable()
export class TableService {
    public detailTable: Table;
    public TableId
    //
    public listTable = []
    numpro = 0;

    readonly rootURL = 'http://localhost:57833';
    // Get product from Home
    productlist$: Observable<any[]>;
    // private productlistSubject = new Subject<any[]>();
    private productlistSubject: BehaviorSubject<any[]>;

    constructor(private http: HttpClient) {
        // ServiceTable nhận products as Obser và có tên là product listdưới dạng [] và tab Component sẽ nhận
        this.productlistSubject = new BehaviorSubject<any[]>([]);
        this.productlist$ = this.productlistSubject.asObservable();
    }

    getProductlist(data: any) {
        console.log(data);
        this.productlistSubject.next(data);
    }
    getTable(){
        return this.http.get(this.rootURL + API.TABLE.GET_TABLES);
    }
    getProduct(): Observable<Table[]> {
        return of(dataTable);
    }

    getNumpro(num) {
        this.numpro = num;
        console.log('Num', this.numpro);
    }
}
