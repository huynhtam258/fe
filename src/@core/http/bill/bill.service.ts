import { Injectable } from '@angular/core';
import { Product } from 'src/@core/models/product';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Table } from 'src/@core/models/table';
import { TableService } from '../table/table.service';
import { API } from '../config/api';
import { queryParams } from 'src/@shared/utils/queryParams';

@Injectable()
export class BillService {
  products: any[] = [];
  nameTable: String;
  billTotal = 0;
  listproduct = [];
  table: Table;
  sanphamdadat = 0;
  tongsotien = 0;
  numberphone = '';
  private productAddedSource = new Subject<any>();
  productAdded$ = this.productAddedSource.asObservable();
  readonly rootURL = 'http://localhost:57833';
  constructor(private router: Router, private http: HttpClient, private selectedTable: TableService) { }
  addProductToBill(product, idtable) {
    let exists = false;
    const price = product.ProductPrice;
    this.billTotal += price;
    // Search this product on the cart and increment the quantity
    this.products = this.products.map(_product => {
      if (_product.product.ProductId === product.ProductId) {
        _product.quantity++;
        exists = true;
      }
      return _product;
    });
    // Add a new product to the cart if it's a new product
    if (!exists) {
      product.price = price;
      this.products.push({
        product: product,
        quantity: 1
      });
    }
    this.productAddedSource.next({ products: this.products, billTotal: this.billTotal });
  }

  removeProductFromBill(product) {
    // Search this product on the cart and increment the quantity
    this.products = this.products.map(_product => {
      if (_product.product.ProductId === product.ProductId) {
        if (_product.quantity > 0) {
          this.products = this.products.slice(_product, 1);
          _product.quantity--;
        }
      }
      return _product;
    });
    this.productAddedSource.next({ products: this.products, billTotal: this.billTotal });
  }

  getBill(productsList) {
    this.listproduct = productsList;
    // console.log(this.listproduct);
  }
  postDetailBill(bill) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.post(this.rootURL + API.DETAILBILL.POST_BILL(queryParams(bill)), { heads: reqHeader });
  }
  postBill(bill) {
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.post(this.rootURL + API.BILL.POST_BILL(queryParams(bill)), { heads: reqHeader });
  }
  postCustomer(customer){
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.post(this.rootURL + API.CUSTOMER.POST_CUSTOMER(queryParams(customer)), { heads: reqHeader });
  }
  getBillADay(data){
    const reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded', 'No-Auth': 'True' });
    return this.http.get(this.rootURL + API.BILL.GET_DETAIL_BY_DAY(queryParams(data)));
  }
  getTotalMoneyADay(data){
    return this.http.get(this.rootURL + API.BILL.GET_TOTAL_MONEY_A_DAY(queryParams(data)));
  }
  getDetailBillByIdBill(id){
    return this.http.get(this.rootURL + API.DETAILBILL.GET_DETAIL_BY_ID_BILL(queryParams(id)));
  }
  getSoluong(soluong, sotien) {
    this.sanphamdadat = soluong;
    this.tongsotien = sotien;
    // console.log(this.sanphamdadat);
  }

}
