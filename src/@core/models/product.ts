export class Product {
    ProductId: any;
    ProductName?: String;
    ProductInfo?: String;
    ProductImage?: String;
    ProductPrice?: Number;
    TypeProductID?: Number;
}
