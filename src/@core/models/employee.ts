export class Employee {
    IDEmployee: Number;
    NameEmployee: String;
    PhoneEmployee: String;
    GenderEmployee: Boolean;
    AddressEmployee: String;
    DateEmployee: Date;
    RoleEmployee: Boolean;
    IDPosition: Number;
}
