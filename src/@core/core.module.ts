import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { ProductService, LoginService } from './http';
// import { NameInterceptor } from './http/config/http-interceptor';
import { TableService } from './http/table/table.service';
// import { TokenService } from './http/token/token.service';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        LoginService,
        ProductService,
        TableService
      ]
    };
  }
}
